#!/usr/bin/python3

# Invoke the present script with a single command-line argument.
# That argument should be the path to a directory which is an unzipped US
# Census Bureau TIGER file, some examples of which can be found at
# https://www2.census.gov/geo/tiger/GENZ2018/shp/ 

# The web point of access for files of this type (in general) seems to be
# https://www.census.gov/geographies/mapping-files/time-series/geo/carto-boundary-file.html

from struct import *
import sys
from geopy.distance import distance
from dbfread import DBF
from os.path import isdir, abspath, basename

def analyze(dirpath):
    base = basename(abspath(dirpath))
    shp = open(dirpath+"/"+base+".shp", "rb")
    shp.seek(24)
    file_length = unpack(">i", shp.read(4))[0]
    # print(file_length)
    file_length *= 2
    shp.seek(32)
    shape_type = unpack("<i", shp.read(4))[0]
    shp.seek(100)
    edges = {}
    borders = {}
    border_lengths = {}
    internal_perimeter = 0
    for record in DBF(dirpath+"/"+base+".dbf"):
        record_number = unpack(">i", shp.read(4))[0]
        content_length = unpack(">i", shp.read(4))[0]
        shp.read(4)
        if shape_type == 5: # polygon
            unpack("dddd", shp.read(32))
            num_parts = unpack("<i", shp.read(4))[0]
            num_points = unpack("<i", shp.read(4))[0]
            parts = [unpack("i", shp.read(4))[0] for _ in range(0, num_parts)]
            p1 = unpack("dd", shp.read(16))
            for i in range(1, num_points):
                p2 = unpack("dd", shp.read(16))
                state = record["STATEFP"]
                if state == "26": # Michigan
                    session = record["CDSESSN"]
                    district = record["CD"+session+"FP"]
                    if (p2, p1) in edges:
                        other = edges[(p2, p1)][0][0]
                        segment_length = distance(reversed(p1), reversed(p2)).miles
                        if (district, other) in borders:
                            borders[(district, other)].append((p1, p2))
                            border_lengths[(district, other)] += segment_length
                            internal_perimeter += segment_length
                        else:
                            borders[(district, other)] = []
                            border_lengths[(district, other)] = segment_length
                        edges[(p2, p1)].append([district, i])
                    else:
                        edges[(p1, p2)] = [[district, i]]
                p1 = p2
        else:
            print("record of a different type found.")
    print(str(border_lengths))
    print(str(len(border_lengths))+ " district adjacencies")
    print("total internal perimeter is "+str(internal_perimeter))

for arg in sys.argv[1:]:
    # TODO: find out how to parse directory pathname
    # TODO: write to sqlite instead of csv
    # TODO: https://github.com/olemb/dbfread
    # TODO: https://github.com/rafalsamborski/wgs84/blob/master/wgs84.py
    if (isdir(arg)):
        analyze(arg)
    else:
        print(arg+" is not a directory")
