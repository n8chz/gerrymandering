#!/usr/bin/python3

from struct import *
import sys

def open_file(file_name):
    try:
        return open(file_name, "rb")
    except:
        return open(file_name+".shp", "rb")

def analyze(shp):
    shp.seek(24)
    file_length = unpack(">i", shp.read(4))[0]
    # print(file_length)
    file_length *= 2
    shp.seek(32)
    shape_type = unpack("<i", shp.read(4))[0]
    shp.seek(100)
    print("record_number,long1,lat1,long2,lat2")
    while shp.tell() < file_length:
        record_number = unpack(">i", shp.read(4))[0]
        content_length = unpack(">i", shp.read(4))[0]
        shp.read(4)
        if shape_type == 5: # polygon
            unpack("dddd", shp.read(32))
            num_parts = unpack("<i", shp.read(4))[0]
            num_points = unpack("<i", shp.read(4))[0]
            parts = [unpack("i", shp.read(4))[0] for _ in range(0, num_parts)]
            points = [unpack("dd", shp.read(16)) for _ in range(0, num_points)]
            for i in range(1, num_points):
                print(",".join(map(str,(record_number,)+points[i-1]+points[i])))
            polygon = [points[i:j] for i, j in zip(parts, parts[1:]+[None])]
            # print(polygon)

        else:
            print("record of a different type found.")

for arg in sys.argv[1:]:
    # TODO: find out how to parse directory pathname
    # TODO: write to sqlite instead of csv
    shp = open_file(arg) 
    analyze(shp)
