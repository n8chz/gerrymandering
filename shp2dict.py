#!/usr/bin/python3

from struct import *
import sys

def open_file(file_name):
    try:
        return open(file_name, "rb")
    except:
        return open(file_name+".shp", "rb")

def analyze(shp):
    shp.seek(24)
    file_length = unpack(">i", shp.read(4))[0]
    # print(file_length)
    file_length *= 2
    shp.seek(32)
    shape_type = unpack("<i", shp.read(4))[0]
    shp.seek(100)
    edges = {}
    borders = {}
    while shp.tell() < file_length:
        record_number = unpack(">i", shp.read(4))[0]
        content_length = unpack(">i", shp.read(4))[0]
        shp.read(4)
        if shape_type == 5: # polygon
            unpack("dddd", shp.read(32))
            num_parts = unpack("<i", shp.read(4))[0]
            num_points = unpack("<i", shp.read(4))[0]
            parts = [unpack("i", shp.read(4))[0] for _ in range(0, num_parts)]
            p1 = unpack("dd", shp.read(16))
            for i in range(1, num_points):
                p2 = unpack("dd", shp.read(16))
                if (p2, p1) in edges:
                    other = edges[(p2, p1)][0][0]
                    if (record_number, other) in borders:
                        borders[(record_number, other)].append((p1, p2))
                    else:
                        borders[(record_number, other)] = []
                    edges[(p2, p1)].append([record_number, i])
                else:
                    edges[(p1, p2)] = [[record_number, i]]
                p1 = p2
        else:
            print("record of a different type found.")
    print(str(borders))

for arg in sys.argv[1:]:
    # TODO: find out how to parse directory pathname
    # TODO: write to sqlite instead of csv
    # TODO: https://github.com/olemb/dbfread
    # TODO: https://github.com/rafalsamborski/wgs84/blob/master/wgs84.py
    shp = open_file(arg) 
    analyze(shp)
